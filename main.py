from mesgex.main import ServerApp


if __name__ == '__main__':
    serverApp = ServerApp(server_listening_addr=('localhost', 60607))
    serverApp.run_service()
